package desafiogrupal;


public class desafio13_prog01 {

    public static void main(String[] args) {

        //Declaración de variables
        int dimension = 100;
        int[] num = new int[dimension];
        int numAleatorio;

        //Carga números aleatorios entre 1 y 100 con un bucle FOR
        for (int i = 0; i < num.length; i++) {
            do {
                numAleatorio = generaNumeroAleatorio(1, 100);
            } while (comprobarSiContiene(num, i, numAleatorio));
            num[i] = numAleatorio;
        }
        //Muestra los numeros almacenados en el arreglo con un bucle FOR-EACH
        System.out.println("Array de números aleatorios desde el 1 al 100");
        for (int array:num) {
            System.out.print(array+"  ");
        }
    }
    //Genera un numero aleatorio entre el minimo y el maximo, includo el maximo y el minimo
    public static int generaNumeroAleatorio(int minimo, int maximo) {
        return (int) Math.floor(Math.random() * (maximo - minimo + 1) + (minimo));
    }
    //Comprueba si el numero ya existe en el arreglo
    public static boolean comprobarSiContiene(int[] numeros, int indice, int valor) {
        for (int i = 0; i < indice; i++) {
            if (numeros[i] == valor) {
                return true;
            }
        }
        return false;
    }
}



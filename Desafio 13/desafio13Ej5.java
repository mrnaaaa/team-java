package TeamJava;
import java.util.Scanner;
public class desafio13Ej5 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Realice un programa que genere una matriz de 3x3 con n�meros ingresados por el usuario por medio de la consola. 
		Una vez terminada la carga de los elementos se debe mostrar primero la matriz cargada con los datos iniciales y
		luego la matriz con los datos ordenados de mayor a menor.*/
		/*Scanner para guardar los numeros ingresados por pantalla que se utilizaran
		 * para posteriormente cargar la matriz que va a ser ordenada. 
		 */
		Scanner consola = new Scanner(System.in); 
		int matriz[][] = new int[3][3];
		int aux[]= new int [9]; //Un  arreglo donde vamos a almacenar toda la matriz
		int cont=0; //Para recorrer el arreglo
		int auxiliar;
		for (int x=0; x < matriz.length; x++) {  //Cargamos la matriz el primer for recorre las filas y el segundo las columnas
			  for (int y=0; y < matriz[x].length; y++) {
			    System.out.println("Introduzca el elemento [" + x + "," + y + "]");
			    matriz[x][y] = consola.nextInt();
			    aux[cont]=matriz[x][y];
			    cont ++;
			  }
			}
		System.out.println("\n Mostramos la matriz cargada. \n"); 
	// Ahora vamos a mostrar la matriz cargada en forma de matriz(vale la redundancia).
		for (int x=0; x < matriz.length; x++) {
			  for (int y=0; y < matriz[x].length; y++) {
			    System.out.print(" " +matriz[x][y]);
			  }
			  System.out.print("\n");
			}
		//Procedemos a ordenar la matriz.
		for (int x=0;x<9;x++) {
			for (int y= x+1;y<9;y++) {
				if(aux[x]<aux[y]) {
					auxiliar=aux[x];
					aux[x]=aux[y];
					aux[y]=auxiliar;
				}
			}
		}
		//Cargamos el arreglo ordenado en la matriz 
		cont =0;
		for (int x=0; x < matriz.length; x++) {
			  for (int y=0; y < matriz[x].length; y++) {
				  matriz[x][y]=aux[cont];
				  cont++;
			  }
			}
		System.out.println("\n Mostramos la matriz ordenada \n");
		//Mostramos la matriz ordenada
		for (int x=0; x < matriz.length; x++) {
			  for (int y=0; y < matriz[x].length; y++) {
			    System.out.print(" " +matriz[x][y]);
			  }
			  System.out.print("\n");
			}
	}
	//TeamJava :D
}
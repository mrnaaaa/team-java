package Desafio13;

import java.util.Arrays;

public class Ejer13_4 {
    private static int[][] matriz = new int[3][3];


    public static void cargar_matriz(){
        int aleatorio;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                aleatorio = (int) (Math.random() * 100 + 1);
                matriz[i][j] = aleatorio;
            }
        }
    }

    public static void mostrar_matriz(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.print("\n");
        }
    }

    public static void ordenar_matriz(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                for(int x = 0; x <3; x++){
                    for (int y = 0; y < 3; y++){
                        if (matriz[i][j] <  matriz[x][y]){
                            int t = matriz [i][j];
                            matriz[i][j] = matriz[x][y];
                            matriz[x][y] = t;
                        }
                    }
                }
            }
        }
    }
    public static void main(String[] args) {

        cargar_matriz();
        System.out.println("Matriz cargada");
        mostrar_matriz();
        System.out.println("Matriz ordenada");
        ordenar_matriz();
        mostrar_matriz();


    }
}
